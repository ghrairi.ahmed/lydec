import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {NgbCarouselConfig} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-signin-signup',
  templateUrl: './signin-signup.component.html',
  styleUrls: ['./signin-signup.component.css']
})
export class SigninSignupComponent implements OnInit {
  signinsignup = 'signin';
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private config: NgbCarouselConfig) {
    this.router.events.subscribe((res) => {
      if (this.router.url === '/signin') {
        this.signinsignup = 'signin';
      } else if (this.router.url === '/signup') {
        this.signinsignup = 'signup';
      }
    });
    config.interval = 3000;
    config.wrap = true;
    config.keyboard = false;
    config.pauseOnHover = false;
    config.showNavigationArrows = false;
  }

  ngOnInit() {
    this.router.navigate(['auth', 'signin']);
      }

  changeform() {
    this.router.navigate(['auth', this.signinsignup]);
  }
}
