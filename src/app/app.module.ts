import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material/input';
import {MatCheckboxModule, MatIconModule, MatSelectModule} from '@angular/material';
import {MatRadioModule} from '@angular/material/radio';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { SigninComponent } from './signin/signin.component';
import { SigninSignupComponent } from './signin-signup/signin-signup.component';
import { SignupComponent } from './signup/signup.component';
import {NgxCaptchaModule} from 'ngx-captcha';
import {StorageServiceModule} from 'ngx-webstorage-service';
import {LocalStorageService} from './local-storage.service';
import { HomeComponent } from './home/home.component';
import { CompteComponent } from './compte/compte.component';
import { HideemailPipe } from './hideemail.pipe';
import { CompteproComponent } from './comptepro/comptepro.component';
@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    SigninSignupComponent,
    SignupComponent,
    HomeComponent,
    CompteComponent,
    HideemailPipe,
    CompteproComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatIconModule,
    MatRadioModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    NgxCaptchaModule,
    MatCheckboxModule,
    StorageServiceModule,
  ],
  providers: [LocalStorageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
