import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SigninSignupComponent} from './signin-signup/signin-signup.component';
import {SigninComponent} from './signin/signin.component';
import {SignupComponent} from './signup/signup.component';
import {HomeComponent} from './home/home.component';
import {CompteComponent} from './compte/compte.component';
import {CompteproComponent} from './comptepro/comptepro.component';

const routes: Routes = [
  { path: '', redirectTo: 'auth/signin', pathMatch: 'full' }
  , {
  path: 'auth', component: SigninSignupComponent , children: [{
    path: 'signin',
    component: SigninComponent
  },
    {
      path: 'signup',
      component: SignupComponent
    }]
},
  {
    path: 'home', component: HomeComponent , children: [{
      path: 'compte', component: CompteComponent
    },
      {
      path: 'comptepro', component: CompteproComponent,
    }]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
