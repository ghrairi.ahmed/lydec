import {Inject, Injectable} from '@angular/core';
import {LOCAL_STORAGE, StorageService} from 'ngx-webstorage-service';

@Injectable({
  providedIn: 'root'
})
@Injectable()
export class LocalStorageService {
  STORAGE_KEY = 'Auth';
  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) { }
  public storeAuthdetails(email: string, password: string): void {
    let Authdetails = this.storage.get(this.STORAGE_KEY) || {};
    Authdetails = {
      email,
     password
    };
    this.storage.set(this.STORAGE_KEY, Authdetails);
    console.log(this.storage.get(this.STORAGE_KEY) || 'LocaL storage is empty');
  }
  public getAuthDetails(): any {
    return this.storage.get(this.STORAGE_KEY) || {};
  }
}
