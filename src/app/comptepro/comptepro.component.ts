import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-comptepro',
  templateUrl: './comptepro.component.html',
  styleUrls: ['./comptepro.component.css']
})
export class CompteproComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  prevform() {
    this.router.navigate(['home', 'compte']);
  }

  confirform() {
  }
}
