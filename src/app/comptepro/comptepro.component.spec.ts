import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompteproComponent } from './comptepro.component';

describe('CompteproComponent', () => {
  let component: CompteproComponent;
  let fixture: ComponentFixture<CompteproComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompteproComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompteproComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
