import { Component, OnInit } from '@angular/core';
import {LocalStorageService} from '../local-storage.service';
import {AuthLoginInfo} from './login-info';
import {Router} from '@angular/router';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  form: any = {};
  rememberme = false;
  closeResult: string;
  str: string;
  res: string;
  emailformodal = '';
  private loginInfo: AuthLoginInfo;
  constructor(private localStorageService: LocalStorageService, private router: Router, private modalService: NgbModal) { }

  ngOnInit() {
    this.loginInfo = this.localStorageService.getAuthDetails();
    this.form.email = this.loginInfo.email;
    this.form.password = this.loginInfo.password;
  }
  connect() {
    console.log(this.form);
    if (this.rememberme) {
      this.localStorageService.storeAuthdetails(this.form.email, this.form.password);
    }
    if (this.form.email !== '' && this.form.password !== '') {
      this.router.navigate(['home', 'compte']);
    }
  }

  onClickForgetPassword(content) {
    this.loginInfo = this.localStorageService.getAuthDetails();
    this.emailformodal = this.loginInfo.email;
    console.log(this.emailformodal);
    this.modalService.open(content, {size: 'sm', centered: true}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;

    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  confirmForgetPassword() {
    this.modalService.dismissAll();
  }
}
