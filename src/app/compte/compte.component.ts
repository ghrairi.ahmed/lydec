import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-compte',
  templateUrl: './compte.component.html',
  styleUrls: ['./compte.component.css']
})
export class CompteComponent implements OnInit {
  typepersonne = 'personnemorale';
  constructor(private router: Router) { }

  ngOnInit() {
  }

  changecompteform() {
  }

  nextform() {
    this.router.navigate(['home', 'comptepro']);
  }
}
